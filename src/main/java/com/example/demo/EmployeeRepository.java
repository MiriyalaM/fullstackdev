package com.example.demo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

	List<Employee> findByFirstName(String firstName);
	List<Employee> findByFirstNameAndLastName(String firstName,String lastName);
	List<Employee> findByFirstNameOrLastName(String firstName,String lastName);
	
	
	
}
